#!/usr/bin/perl -w
# Global string methods

use HTML::Entities;
use Encode;

# Trim a string
sub trim {
  my ($str) = @_;
  return if !defined($$str);
  $$str =~ s/^\s+//;
  $$str =~ s/\s+$//;
}

# Make a string title case
sub title_case {
  my ($str) = @_;
  $str =~ s/(\S+)/\u\L$1/g;
  return $str;
}

# Make text database-safe
sub convert_text {
  $_[0] = Encode::decode('UTF-8', $_[0]) if $_[0] !~ /[^\x00-\x7f]/;
  return encode_entities($_[0]);
}
# Reverse for display
sub decode_text {
  return Encode::encode('UTF-8', decode_entities($_[0]));
}

# Convert undefined values to NULL, otherwise wrap around quotes
sub check_for_null {
  my ($txt) = @_;
  if (defined($$txt) && $$txt !~ m/^\s*$/) {
    $$txt = "'$$txt'";
  } else {
    $$txt = 'NULL';
  }
}

# Convert a string with encoded characters into their latin equivalent (e.g., é to e)
sub convert_latin1 {
  my ($str) = @_;
  # Convert in to a HTML char we can process
  my $str_enc = convert_text($str);
  # Then determine the HTML components
  my @enc = ($str_enc =~ m/(&[a-z]+;)/gi);
  # And fix in the original string
  my %done = ( );
  foreach my $enc (@enc) {
    next if defined($done{$enc});
    my $enc_dec = decode_text($enc);
    my ($enc_char) = ($enc =~ m/^&(.)/);
    $str =~ s/$enc_dec/$enc_char/g;
    $done{$enc} = 1;
  }
  return $str;
}

# Return true to pacify the compiler
1;
