# Data Helper Scripts

Common scripts for data processing used across multiple repositories (predominantly Sports and Fantasy).

## Status

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Authors

* **Thierry Draper** - [@ThierryDraper](https://gitlab.com/thierrydraper)

## Contribute

Please read [CONTRIBUTING.md](https://gitlab.com/debear/scripts/data-helpers/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## License

This project is made available under the terms of the GNU Affero General Public License v3.0. Please see the [LICENSE](https://gitlab.com/debear/scripts/data-helpers/blob/master/LICENSE) file for details.

GNU AGPLv3 © [Thierry Draper](https://gitlab.com/thierrydraper)
