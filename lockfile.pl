#!/usr/bin/perl -w
# Methods for defining, checking and ending lock files

use DBI;

#
# Setters
#
# Process starting (checking we are not starting after an unrecovered failure)
sub lockfile_start {
  my ($app) = @_;

  # Connect to the database to determine if we're locked and/or update the start time
  my $dbh = lockfile__dbh();
  my $sth;

  # Check the lockfile (unless we're told to ignore it, as we've resolved the problem)
  $skip_lockfile = grep(/^--skip-lockfile$/, @ARGV);
  if (!defined($skip_lockfile) || !$skip_lockfile) {
    $sth = $dbh->prepare('SELECT IFNULL(last_started, "0000-00-00 00:00:00") > IFNULL(last_finished, "0000-00-00 00:00:00") AS is_locked FROM LOCKFILE_STAMPS WHERE app = ?;');
    $sth->execute($app);
    my ($is_locked) = $sth->fetchrow_array;

    # We can't carry on if we're locked...
    if ($is_locked) {
      print STDERR "Unable to proceed due to lock file checks. If trying to run after a lockfile error, please use the --skip-lockfile flag.\n";
      exit $config{'exit_codes'}{'failed-prereq'};
    }
  }

  # If we're carrying on, flag
  $sth = $dbh->prepare('INSERT INTO LOCKFILE_STAMPS (app, last_started) VALUES (?, NOW()) ON DUPLICATE KEY UPDATE last_started = VALUES(last_started);');
  $sth->execute($config{$mode}{'app'});

  # Disconnect from the database
  undef $sth if defined($sth);
  $dbh->disconnect if defined($dbh);
}

# Process complete
sub lockfile_finish {
  my ($app) = @_;
  my $dbh = lockfile__dbh();
  my $sth = $dbh->prepare('UPDATE LOCKFILE_STAMPS SET last_finished = NOW() WHERE app = ?;');
  $sth->execute($app);
  undef $sth if defined($sth);
  $dbh->disconnect if defined($dbh);
}

#
# Synchronise the timestamp table to live
#
sub lockfile_sync {
  $config{$mode}{'daily_tunnel'} = 0 if !defined($config{$mode}{'daily_tunnel'});
  command('/var/www/debear/bin/git-admin/server-sync lockfile_stamps live' . ($config{$mode}{'daily_tunnel'} ? ' --daily-tunnel' : ''), '/dev/null', '/dev/null');
}

#
# DBH
#
sub lockfile__dbh {
  return (DBI->connect('dbi:mysql:database=' . $config{'db_admin'}, $config{'db_admin'}, $config{'db_pass'})
    or die("Unable to connect to $config{'db_admin'}: " . DBI::errstr));
}

#
# Getters
#
# Started
sub lockfile_last_started {
  my ($app) = @_;
  my $dbh = lockfile__dbh();
  my $sth = $dbh->prepare('SELECT last_started AS dt FROM LOCKFILE_STAMPS WHERE app = ?;');
  $sth->execute($app);
  my ($dt) = $sth->fetchrow_array;
  undef $sth if defined($sth);
  $dbh->disconnect if defined($dbh);
  return $dt;
}
# Finished
sub lockfile_last_finished {
  my ($app) = @_;
  my $dbh = lockfile__dbh();
  my $sth = $dbh->prepare('SELECT last_finished AS dt FROM LOCKFILE_STAMPS WHERE app = ?;');
  $sth->execute($app);
  my ($dt) = $sth->fetchrow_array;
  undef $sth if defined($sth);
  $dbh->disconnect if defined($dbh);
  return $dt;
}

# Return true to pacify the compiler
1;
