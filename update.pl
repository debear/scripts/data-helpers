#!/usr/bin/perl -w
# Common update_* methods

use Data::Dumper;
use Capture::Tiny qw(capture capture_stdout capture_stderr);

our %config;
$config{'debug'} = grep(/^--debug$/, @ARGV);
$config{'download_only'} = grep(/^--download$/, @ARGV); # Set to only download data, rather than parse and import it as well
$config{'is_unattended'} = grep(/^--unattended$/, @ARGV); # No user interaction expected
$config{'is_historical'} = grep(/^--historical$/, @ARGV); # Importing historical data (rather than current season by date)
$config{'is_completing'} = grep(/^--complete$/, @ARGV); # Finish a previously started run, requiring manual intervention
our @cmd_list;
# Standard logging base
$config{'log_dir'} = "$config{'base_dir'}/_logs/" if !defined($config{'log_dir'});

#
# End the script, tidying up log files
#
sub end_script {
  my ($exit_code) = @_;

  # Archive the log files
  if (!$config{'debug'} && -e $config{'log_dir'} && -d $config{'log_dir'} && defined($config{'log_date'})) {
    system("cd $config{'log_dir'}/.. && /usr/bin/tar -czf $config{'log_date'}.tar.gz $config{'log_date'}");
    unlink glob("$config{'log_dir'}/*");
    rmdir $config{'log_dir'};
  }

  # Inform user
  print "\n" . 'Run completed at ' . `date` . "\n";

  # Any debug data to display?
  our @cmd_list;
  print Dumper(\@cmd_list) if $config{'debug'};

  # Ensure we have an exit code...
  $exit_code = 'okay' if !defined($exit_code);
  $exit_code = 'unknown' if !defined($config{'exit_codes'}{$exit_code});

  # Any custom tidy-up code to run?
  end_script_custom($exit_code) if defined(&end_script_custom);

  # Return with appropriate exit code
  exit $config{'exit_codes'}{$exit_code};
}

# Handle an early exit, pre-calcing some details before calling end_script
sub end_script_early {
  $config{'exit_status'} = 'unknown' if !defined($config{'exit_status'}); # Generic error exit status
  end_script($config{'exit_status'});
}

#
# Run a command, capturing and processing output/errors
#
sub command {
  my ($cmd, $stdout, $stderr) = @_;

  # If we're debugging, store the command and move on
  if ($config{'debug'}) {
    our @cmd_list;
    push @cmd_list, $cmd;
    return;
  }

  # Build the command
  my $full_cmd = $cmd;

  # Parse the arguments to determine how we handle STDOUT/STDERR
  my $capture;
  parse_fh_arg('STDOUT', $stdout, \$full_cmd, \$capture);
  parse_fh_arg('STDERR', $stderr, \$full_cmd, \$capture);

  # Run
  our $rc = 0;
  if (!defined($capture)) {
    run_command($full_cmd);
  } else {
    # Run via Capture::Tiny
    my $o_stdout = '';
    my $o_stderr = '';
    if ($capture eq 'BOTH') {
      ($o_stdout, $o_stderr) = capture { run_command($full_cmd) };
    } elsif ($capture eq 'STDOUT') {
      ($o_stdout) = capture_stdout { run_command($full_cmd) };
    } elsif ($capture eq 'STDERR') {
      ($o_stderr) = capture_stderr { run_command($full_cmd) };
    }
    # Process output
    print $stdout $o_stdout if defined($stdout) && ref($stdout) ne 'SCALAR';
    print $stderr $o_stderr if defined($stderr) && ref($stderr) ne 'SCALAR';
  }

  # Check for errors
  if (defined($stderr) && -e $stderr) {
    # Error = Line not starting with #
    my $lines = `grep '^[^#]' $stderr | wc -l`; chomp($lines);
    if ($lines) {
      my $file = basename($stderr);
      my $errors = ($lines > 10 ? "(Last 10 lines)\n" : '') . `grep '^[^#]' $stderr | tail`; chomp($errors);
      $errors =~ s/\n/\n> /g;
      my $s = ($lines == 1 ? '' : 's');
      print STDERR "Error$s logged in '$file' ($lines line$s):\n> $errors\n";
      end_script_early();
    }
  }

  # Silent fails, early aborts that aren't errors per-se
  if ($rc == 97) {
    end_script();
  # Generic error, where no details were passed to STDERR
  } elsif ($rc) {
    print STDERR "An error occurred (code: $rc), exiting!\n";
    end_script_early();
  }
}

sub parse_fh_arg {
  my ($fh, $arg, $full_cmd, $capture) = @_;

  # Only parsing if something passed
  return if !defined($arg);

  # To a file
  if (ref(\$arg) eq 'SCALAR') {
    my $fh_num = ($fh eq 'STDOUT' ? '' : '2');
    $$full_cmd .= " $fh_num>>$arg";
  # To a filehandle
  } elsif (!defined($$capture)) {
    $$capture = $fh; # Just $fh
  } else {
    $$capture = 'BOTH'; # Both STDERR and STDERR
  }
}

sub run_command {
  system $_[0];
  # Get exit status
  our $rc = ($? >> 8);
}

sub done {
  my ($pad) = @_;
  my $p = ' ' x $pad;
  print "$p\[ Done ]\n";
}

#
# Ask the user a question
#
sub question_user {
  my ($question, $valid, $default) = @_;

  # Display the question
  print $question . ': ';
  my $response = <STDIN>; chomp($response);
  while ($response !~ m/^$valid$/i) {
    # Set a default if no value passed
    if ($response =~ m/^\s*$/) {
      $response = $default;
    } else {
      print " - '$response' is not a valid response.  Please try again: ";
      $response = <STDIN>; chomp($response);
    }
  }

  # Return the final value
  return $response;
}

# Return true to pacify the compiler
1;
