#!/usr/bin/perl -w
# Global process flow methods

# Display output?
sub check_disp {
  my ($check, $check_cmp) = @_;
  $check_cmp = $config{'disp'} if !defined($check_cmp);
  
  # If set to all, no need to continue
  return 1
    if $check_cmp == -1;

  # Convert scalar inputs to a list
  my @checks;
  if (ref(\$check) eq 'SCALAR') {
    @checks = ( $check );
  } else {
    @checks = @$check;
  }

  # Loop through our individual checks
  foreach my $c (@checks) {
    my $m = 0; # Assume false, until proven otherwise

    # If we've been given a scalar, check it matches exactly
    if (ref(\$check_cmp) eq 'SCALAR') {
      $m = ($check_cmp == $c);
    
    # Otherwise if an array then check it is in the list
    } elsif (ref($check_cmp) eq 'ARRAY') {
      $m = (grep(/^$c$/, @{$check_cmp}));
    }
    
    # If a match, return so... (loop-and-a-half fix...)
    return 1 if $m;
  }
  
  # If unknown type, return false
  return 0;
}

# Return true to pacify the compiler
1;
