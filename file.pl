#!/usr/bin/perl -w
# Global file methods

# Load contents of a file
sub load_file {
  my ($file) = @_;
  
  # Load the file
  my $contents;
  open FILE, "<$file";
  my @contents = <FILE>;
  close FILE;
  $contents = join('', @contents);
  @contents = (); # Garbage collect...
  
  return $contents;
}

# Return true to pacify the compiler
1;
