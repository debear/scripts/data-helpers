#!/usr/bin/perl -w
# Common library for downloading and verifying files
use File::Basename;
use HTTP::Request;
use LWP::UserAgent;
use JSON;

# Options:
# clobber: remove if already exists
# skip-today: do not download if exists and was modified today
# debug: do not actually run
# gzip: download as gzipped content

# Returns:
# - File ouput:
#   0: Fail
#   1: Success
#   2: Skipped (exists from today)
#   3: Skipped (debug)
# - Content output:
#   (String)

sub download {
  my ($url, $local, $opt) = @_;

  #
  # Pre-flight checks
  #

  # Convert no $local into a standard version
  $local = '-'
    if !defined($local) || !$local;

  # Clean the URL
  $url =~ s/&/\\&/g
    if $url !~ /[^\\]&/;

  # Single check for gzip flag
  my $is_gzip = (defined($$opt{'gzip'}) && $$opt{'gzip'});

  # Pre-download file checks (skipped if STDOUT)
  my $is_file = ($local ne '-');
  if ($is_file) {
    # Gzipped files should have .gz ext
    $local .= '.gz'
      if $is_gzip && $local !~ /\.gz$/;

    # Ensure the destination folder exists
    my $dir = dirname($local);
    mkpath($dir)
      if ! -e $dir;

    # Skip if already downloaded?
    if (-e $local && defined($$opt{'skip-today'}) && $$opt{'skip-today'}) {
      my @stat = stat $local;
      return 2
        if time2date($stat[9]) eq time2date(time()); # 9 = mtime
    }

    # Clobber mode?
    unlink($local)
      if (-e $local && defined($$opt{'clobber'}) && $$opt{'clobber'});

    # Fallback to making an If-Modified-Since request if the file exists
    # and no other rules for handling existing instances have been defined
    $$opt{'if-modified-since'} = 1
      if (-e $local && !defined($$opt{'skip-today'}) && !defined($$opt{'clobber'}));
  }

  # Skip if debugging, assuming valid download was made (though doesn't work for content output)
  return ($is_file ? 3 : '')
    if defined($$opt{'debug'}) && $$opt{'debug'};

  #
  # Perform the download
  #

  # With some logging
  my %log = ( 'url' => $url, 'local' => $local, 'passes' => [ ], 'custom_gzip' => 0 );

  # Configure the User Agent
  my $ua = LWP::UserAgent->new;
  $ua->timeout(30);
  $ua->agent('Downloader/1.0');

  # Pass twice, the second time deeming it really did probably fail...
  my $passes = 1; my $response;
  for (my $i = 0; $i < $passes; $i++) {
    # Build our request and add any headers
    my $request = HTTP::Request->new(GET => $url);
    $request->header('Accept-Encoding' => 'gzip')
      if $is_gzip;
    if (-e $local && defined($$opt{'if-modified-since'}) && $$opt{'if-modified-since'}) {
      my @stat = stat $local;
      $request->header('If-Modified-Since' => strftime("%a, %d %b %Y %T GMT", gmtime($stat[9]))); # 9 == mtime
    }

    # Run
    $response = ($is_file ? $ua->request($request, $local) : $ua->request($request));

    # Log
    $log{'passes'}[$i] = { 'code' => $response->code, 'headers' => $request->headers->as_string('') };
    if ($is_file) {
      if (-e $local) {
        my @stat = stat $local;
        $log{'passes'}[$i]{'content_length'} = $stat[7]; # 7 == size (in bytes)
      } else {
        $log{'passes'}[$i]{'content_length'} = 'unknown';
      }
    } else {
      $log{'passes'}[$i]{'content_length'} = length($response->content);
    }
    @{$log{'passes'}[$i]{'response_headers'}} = split("\n", $response->headers()->as_string);

    # If we died half way through, clear the file and try again
    my $code = $response->code;
    if (($passes < 5) &&
          (($response->headers()->as_string =~ m/X-Died: read timeout/i)
            || ($response->headers()->as_string =~ m/Client-Aborted: die/i)
            || ($response->headers()->as_string =~ m/Client-Warning: Internal response/i))) {
      unlink $local if $is_file;
      $passes++;
    # Validate, based on status code
    } elsif (!grep("/^$code$/", [200, 304])) {
      # If we've not reached the limit of acceptable errors, let's try again
      if ($response->code == 500 && $passes < 5) {
        $passes++;
      # If we've not reached the limit of acceptable errors, let's try again
      # Cap the number of non-500 errors at 2
      #  (or at least, allow another chance for any error the first time, but only a single non-500 error thereafter)
      } elsif (!$i) {
        $passes = 2;
      # Subsequent fail implies erroneous download
      } else {
        download_log(\%log);
        return ($is_file ? 0 : '');
      }
    }
  }

  # If we're returning content, return it
  if (!$is_file) {
    download_log(\%log);
    return $response->content;
  }

  # There exists the possibility the server didn't act on the gzip header so the downloaded file isn't gzipped.
  # If it isn't, make it so!
  if (defined($$opt{'gzip'}) && $$opt{'gzip'} && $is_file && (-e $local) && !`file $local | grep -F gzip`) {
    $log{'custom_gzip'} = 1;
    my $local_raw = $local; $local_raw =~ s/\.gz$//;
    rename $local, $local_raw;
    `gzip $local_raw`;
  }

  # All good, so return a happy status
  download_log(\%log);
  return 1;

}

sub download_log {
  my ($log) = @_;
  my @now = localtime;
  open(FILE, ">>/var/www/debear/logs/common/perl_download/".sprintf('%04d-%02d-%02d', $now[5]+1900, $now[4] + 1, $now[3]).".log") or die $!;
  print FILE "=> URL: $$log{'url'}; Local: $$log{'local'}; Passes (" . scalar(@{$$log{'passes'}}) . "): " . encode_json($$log{'passes'}) . "; Custom Gzip: $$log{'custom_gzip'}\n";
  close(FILE);
}

# Return true to pacify the compiler
1;
