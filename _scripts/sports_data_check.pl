#!/usr/bin/perl -w
# Check that we have successfully processed the sports data before processing for fantasy

use strict;
use Dir::Self;
use Cwd 'abs_path';
use DBI;

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config = abs_path(__DIR__ . '/../_global/update.pl');
require $config;

# Inform user
print "Checking status of sports sync for '$config{'sport'}':\n";

# Perform checks:
#  1. Happy lockfile (admin db)
my $dbh_a = DBI->connect('dbi:mysql:' . $config{'db_admin'}, $config{'db_admin'}, $config{'db_pass'});
my $query_a = 'SELECT last_started <= last_finished FROM LOCKFILE_STAMPS WHERE app = CONCAT("sports_", ?);';
my $sth_a = $dbh_a->prepare($query_a);
$sth_a->execute($config{'sport'});
my ($lockfile_check) = $sth_a->fetchrow_array;
$lockfile_check = 0 if !defined($lockfile_check);
undef $sth_a if defined($sth_a);
$dbh_a->disconnect if defined($dbh_a);
print "- Lockfile:  [ " . ($lockfile_check ? 'Pass' : 'Fail') . " ]\n";
#  2. No new games (sports db)
my $dbh_b = DBI->connect('dbi:mysql:' . $config{'db_name_sports'}, $config{'db_name_sports'}, $config{'db_pass'});
my $query_b = 'SELECT COUNT(*) = 0 FROM SPORTS_' . uc($config{'sport'}) . '_SCHEDULE WHERE game_date < CURDATE() AND status IS NULL;';
my $sth_b = $dbh_b->prepare($query_b);
$sth_b->execute;
my ($game_check) = $sth_b->fetchrow_array;
$game_check = 0 if !defined($game_check);
undef $sth_b if defined($sth_b);
$dbh_b->disconnect if defined($dbh_b);
print "- Game Data: [ " . ($lockfile_check ? 'Pass' : 'Fail') . " ]\n";

# Status check...
my $status = ( $lockfile_check && $game_check );
print "=> Overall:  [ " . ($status ? 'Pass' : 'Fail') . " ]\n";

# Throw appropriate exit code: okay on success, failed-prereq on error
my $code = ($status ? 'okay' : 'failed-prereq');
print "   Returning code: '$code'\n";
exit $config{'exit_codes'}{$code};
