#!/bin/bash
# Merge multiple log files generated each day in to a single .tar file
dir_script=$(dirname $0)
dir_root=$(realpath "$dir_script/../../..")

# Parse the arguments
legacy=0
while [ "$1" != "" ]; do
  # First argument is the sport
  if [ -z $sport ]; then
    sport=$1
    # Using the sport, determine if we're dealing with a migrated or migrating location
    dir_sport="$dir_root/sites/sports/data/$sport/_logs"
    dir_base="$dir_root/logs/common/sports-sync/$sport"
    if [ ! -e "$dir_base"  ]; then
      dir_base="$dir_sport"
      legacy=1
    fi
  # All the rest are directories
  elif [ -e $dir_sport/$dir ]; then
    dirs="$dirs$1 "
  else
    invalid="$invalid$1 "
  fi
  shift
done

# Validation
if [ -z "$sport" ]; then
  echo "* Missing required argument: sport" >&2
  exit 9
elif [ ! -z "$invalid" ]; then
  echo "* Invalid / Missing directory arguments" >&2
  exit 9
fi

# Loop
today=$(date +'%F')
for dir in ${dirs[@]}; do
  # Daily script
  if [ $legacy -eq 1 ]; then subdir="_$dir"; else subdir="$dir"; fi # Legacy migration
  cd "$dir_base/$subdir"
  for d in $(ls *.log.gz 2>/dev/null | sort | grep -Po '^(20[0-9]{2}-[0-9]{2}-[0-9]{2})' | grep -Fv "$today" | uniq); do
    tar -cf $d.tar $(ls ${d}_*.log.gz)
    rm -rf ${d}_*.log.gz
  done

  # Actual script
  cd "$dir_sport/$dir"
  for d in $(ls *_*.tar.gz 2>/dev/null | sort | grep -Po '^(20[0-9]{2}-[0-9]{2}-[0-9]{2})' | grep -Fv "$today" | uniq); do
    tar -cf $d.tar $(ls ${d}_*.tar.gz)
    rm -rf ${d}_*.tar.gz
  done
done
