#!/usr/bin/perl -w
# Generic automated script, combining various elements of an integration and reporting back

use strict;
use Dir::Self;
use Cwd 'abs_path';
use File::Basename;

# Mode is the script name (as we'll be aliased...)
our $mode = basename($0, '.pl');

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
my $global_dir = abs_path(__DIR__ . "/" . (defined($config{'global_rel'}) ? $config{'global_rel'} : '../') . '_global');
for my $inc ('update.pl', 'automated.pl', 'lockfile.pl', 'log.pl') {
  require "$global_dir/$inc";
}

# Lockfile checks
lockfile_start($config{$mode}{'app'});

# Logging
$config{'log_inc_time'} = $config{$mode}{'log_inc_time'} if defined($config{$mode}{'log_inc_time'});
identify_log_file($config{'log_ext'});

# State we're starting
print "Automated $mode run started at " . `date` . "\n";

#
# Run component parts
#
my $i = 1;
foreach my $script (@{$config{$mode}{'steps'}}) {
  my $msg = "Step $i :: $$script{'label'}"; my $msg_len = length($msg);
  print "$msg\n" . '='x$msg_len . "\n";
  command($$script{'script'}, *MOCKSTDOUT, *MOCKSTDOUT); # Loop STDERR in to STDOUT, as we only want our own STDERR caught
  print "\n";
  $i++;
}

# State that we're finished and close any SSH connection
lockfile_finish($config{$mode}{'app'});

#
# Tidy up and end the script
#
end_script();
