#!/usr/bin/perl -w
# Wrapper script to to the log aggregator, building specifically for the sport we are running as

use strict;
use Dir::Self;
use Cwd 'abs_path';

# Get base config
our %config;
my $config = abs_path(__DIR__ . '/config.pl');
require $config;
$config{'debug'} = grep(/^--debug$/, @ARGV);

# Determine the appropriate scripts we have
my @scripts = ();
foreach my $key (keys %config) {
  push @scripts, $key
    if (ref($config{$key}) eq 'HASH')
      && defined($config{$key}{'steps'})
      && defined($config{$key}{'app'})
      && defined($config{$key}{'log_inc_time'});
}

# Fail if not scripts matched
if (!@scripts) {
  print STDERR "Unable to find any non-daily scripts with logs to aggregate. Aborting.\n";
  exit $config{'exit_codes'}{'failed-prereq'};
}

# Build the command
my $is_legacy = defined($config{'base_dir'});
my $sport = (!$is_legacy ? basename(dirname($0)) : basename($config{'base_dir'})); # $config{'base_dir'} = Legacy migration
my $cmd = dirname(abs_path($0)) . "/aggregate_logs.sh $sport " . join(' ', @scripts);

# Display if debugging
if ($config{'debug'}) {
  print "Command to be run: '$cmd'\n";
  exit;
}
# Otherwise, run and return the same exit code
system $cmd;
my $rc = ($? >> 8);
exit $rc;
