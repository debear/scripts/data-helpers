#!/usr/bin/perl -w
# Global benchmarking methods

use File::Basename;
use Time::HiRes qw(gettimeofday);

#
# Default config
#
$config{'benchmarking'}{'stderr'} = 1; # Write output to STDERR

#
# Worker methods
#

# Record a timestamp
sub benchmark_stamp {
  # No need to run if not enabled
  return if !$_benchmarking{'enabled'};

  my ($label) = @_;
  $label = 'Timer ' . @{$_benchmarking{'keys'}} if !defined($label);

  $_benchmarking{'misc'}{'max_label'} = length($label) if length($label) > $_benchmarking{'misc'}{'max_label'};
  push @{$_benchmarking{'keys'}}, $label;
  $_benchmarking{'stamps'}{$label} = gettimeofday();
}

# Output the results...
sub benchmark_output {
  # No need to run if not enabled
  return if !$_benchmarking{'enabled'};

  my ($label) = @_;
  my $fh = (defined($config{'benchmarking'}{'stderr'}) ? STDERR : STDOUT);
  my $label_first = $_benchmarking{'keys'}[0];
  
  # Add a final counter
  $label = defined($label) ? $label : 'Script End';
  benchmark_stamp($label);
  
  # Formatting: Number of equals signs in output...
  my $run_time = length(int($_benchmarking{'stamps'}{$label} - $_benchmarking{'stamps'}{$label_first}) + 100);
  my $line_max = (2 * ($_benchmarking{'misc'}{'max_label'} + $run_time + 7)) + 17;
  my $head_len = length(basename($0)) + 16;
  my $line_l = int(($line_max - $head_len) / 2) + ((($line_max - $head_len) % 2) ? 1 : 0);
  my $line_r = int(($line_max - $head_len) / 2);

  # Total time
  my $total_time = $_benchmarking{'stamps'}{$_benchmarking{'keys'}[-1]} - $_benchmarking{'stamps'}{$label_first};
  
  # Output - Headline
  print $fh "\n" . ('='x$line_l) . " Benchmarking: " . basename($0) . ' ' . ('='x$line_r) . "\n";
  benchmark_print($_benchmarking{'keys'}[0], $label, $label_first, $total_time, $fh);
  print $fh ('-'x$line_max) . "\n";
  # Output - Breakdown
  my $label_last;
  for (my $i = 0; $i < @{$_benchmarking{'keys'}}; $i++) {
    my $label_cur = $_benchmarking{'keys'}[$i];
    benchmark_print($label_last, $label_cur, $label_first, $total_time, $fh, '  ') if defined($label_last); # if is a way of making 1..(x-1) work
    $label_last = $label_cur;
  }
  # Output - End...
  print $fh ('='x$line_max) . "\n\n";
}
sub benchmark_print {
  my ($a, $b, $start, $total_time, $fh, $lpad) = @_;
  $lpad = '' if !defined($lpad);
  my $fmt_a = ($lpad eq '' ? '' : $_benchmarking{'misc'}{'max_label'});
  my $fmt_b = '-' . ($lpad eq '' ? ((2 * $_benchmarking{'misc'}{'max_label'}) - length($b)) : $_benchmarking{'misc'}{'max_label'});

  # Format for display
  my $str  = '%s%' . $fmt_a . 's => %' . $fmt_b . 's  %.06f';
  my @args = ($lpad, $a, $b, $_benchmarking{'stamps'}{$b} - $_benchmarking{'stamps'}{$a});
  if ($a ne $start) {
    $str .= ' (%.06f)';
    push @args, $_benchmarking{'stamps'}{$b} - $_benchmarking{'stamps'}{$start};
  } else {
    $str .= (' 'x11);
  }
  my $pct = 100 * (($_benchmarking{'stamps'}{$b} - $_benchmarking{'stamps'}{$a}) / $total_time);
  $str .= ' (%s%s%.02f%%)';
  push @args, $pct < 100 ? ' ' : '';
  push @args, $pct < 10 ? ' ' : '';
  push @args, $pct;
  printf $fh "$str\n", @args;
}

#
# Setup
#
sub benchmark_setup {
  # Setup our internal benchmarking 
  our %_benchmarking = ( 'enabled' => grep(/\-\-benchmark$/, @ARGV) ? 1 : 0,
                         'keys' => [ ],
                         'stamps' => { },
                         'misc' => { 'max_label' => 0 } );
  benchmark_stamp('Script Start'); # First opportunity we have to record a start point...
}
benchmark_setup();

#
# Automatically tie up at the end of a completed run
#
END {
  benchmark_output('Script End');
}

# Return true to pacify the compiler
1;
