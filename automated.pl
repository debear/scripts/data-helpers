#!/usr/bin/perl -w
# Tools for handling and processing automated scripts

use DBI;

our %_networking;
require '/var/www/debear/lib/network.pl';
require '/var/www/debear/lib/data_helpers/strings.pl';

# Redirect output to variable for internal processing
$config{'log_ext'} = 'log.gz';
$config{'stdout'} = '';
open MOCKSTDOUT, '>', \$config{'stdout'} or die("Unable to open MOCKSTDOUT");
select MOCKSTDOUT;

#
# Custom handler to end_script
#  - Notify on output via email to support@
#
sub end_script_custom {
  my ($exit_code) = @_;
  chomp($config{$mode}{'email-subject'}); chomp($config{'stdout'});

  ## Email on error? (Remember, exit code '0' => okay..., 97 = silent errors)
  if ($config{'exit_codes'}{$exit_code} && $config{'exit_codes'}{$exit_code} != 97) {
    ssh_tunnel_open();
    # Open MySQL connection
    my $dbh = DBI->connect('dbi:mysql:database=' . $config{'db_common'} . ';host=127.0.0.1;port=' . $_networking{'ssh_tunnel_port'}, $config{'db_user'}, $config{'db_pass'}) or die("Unable to connect to $config{'db_common'}: " . DBI::errstr);
    # Prepare the query and execute
    my $sth = $dbh->prepare('INSERT INTO COMMS_EMAIL (app, email_id, user_id, email_reason, email_from, email_to, email_subject, email_body, email_queued, email_send, email_status) VALUES (?, NULL, 1, CONCAT("auto_", ?), CONCAT("DeBear Support <support@", ?, ">"), CONCAT("DeBear Support <support@", ?, ">"), ?, ?, NOW(), NOW(), "queued");');
    $sth->execute(
      $config{$mode}{'app'},
      $mode,
      $config{'domain'},
      $config{'domain'},
      $config{$mode}{'email-subject'},
      convert_text($config{'stdout'}) . "\n\nExit code: $config{'exit_codes'}{$exit_code} ('$exit_code')"
    );

    # Close connection
    undef $sth if defined($sth);
    $dbh->disconnect if defined($dbh);
    ssh_tunnel_close();
  }

  ## Store log file (using log_dir as we're storing in a single file)
  log_gzip($config{'log_dir'} . '.' . $config{'log_ext'}, $config{'stdout'});

  # In all instances, we should update the live server with the current state of the sync
  lockfile_sync();
}

# Return true to pacify the compiler
1;
