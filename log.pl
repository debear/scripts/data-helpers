#!/usr/bin/perl -w
# Global logging methods
use POSIX;

# Identify the log date (subdir) to use
sub identify_log_date {
  my ($log_arg) = grep { $_ =~ /^--log-dir=/ } @ARGV;
  my @time = localtime();

  # Use what we were passed  
  if (defined($log_arg)) {
    $log_arg =~ s/^--log-dir=//;
    $log_arg =~ s/^["'](.+)["']$/$1/ if $log_arg =~ m/^["'].+["']$/;

  # Based on today's date
  } else {
    $log_arg = sprintf('%04d-%02d-%02d', $time[5] + 1900, $time[4] + 1, $time[3]);
  }

  $config{'log_date'} = $log_arg;
  
  # Custom log dir postfix?
  if (defined($config{'log_dir_postfix'})) {
    $config{'log_date'} .= '.' . $config{'log_dir_postfix'};
  } elsif (defined($config{'log_inc_time'})) {
    $config{'log_date'} .= '_' . strftime($config{'log_inc_time'}, @time);
  }
}

# Idenfity the log file to use
sub identify_log_file {
  my ($ext) = @_;
  $ext = 'tar.gz' if !defined($ext);
  identify_log_date();
  my $extra = ''; my $extra_dot = '';
  while (-e "$config{'log_dir'}/$config{'log_date'}$extra_dot$extra.$ext") {
    if ($extra eq ''){ 
      $extra_dot = '.';
      $extra = 1;
    } else {
      $extra++
    }
  }
  $config{'log_date'} .= "$extra_dot$extra";
  $config{'log_dir'} .= "/$config{'log_date'}";
}

# Convert a Unix timestamp to a Y-m-d date
sub time2date {
  my ($ts) = @_;
  my @parts = localtime($ts);
  
  return sprintf('%04d-%02d-%02d', $parts[5] + 1900, $parts[4] + 1, $parts[3]);
}

# Write gzipped log message
#  - Currently only built for single write, use IO::Compress::Gzip for append functionality?
sub log_gzip {
  my ($file, $msg) = @_;
  
  open GZFH, "| /bin/gzip -c >$file";
  print GZFH $msg;
  close GZFH;
}

# Return true to pacify the compiler
1;
